# Welcome, I'm Kevin

#### I'm a full-stack engineer with a love for all things JavaScript / TypeScript and a passion for functional programming. I believe in empowering those around me and the power of psychological safety. I strive to create readable, maintainable code and to help others do the same.

![My Skills](https://go-skill-icons.vercel.app/api/icons?i=ts,react,reactquery,nodejs,npm,pnpm,vitest,cypress,jest,gitlab,github,githubactions,postgresql,prisma,trpc,vite,next,docker,kubernetes,gleam,nixos,flyio,reactnative,graphql,vscode,nginx,godot,notion,figma,storybook&theme=dark)

[See my full portfolio](https://www.kgroat.dev)
